## MAX CLIQUE SOLVER
### Ineteligencia artificial - Algoritmos Genéticos

Este proyecto pretende hallar la mejor clique posible en base a un algoritmo genético para grafos dados.

---

### Instalación 
1. Ejecute ```./build.sh``` para compilar tanto la libreria como el algoritmo.
2. Debe aparecer el archivo ```maxClique```. Si no tuviera permisos de ejecución ejecute ```chmod +x maxClique```.
3. Ya esta todo listo para lanzar el algoritmo.

---

### Ejecución
Hay distintas formas de ejecutar el algoritmo:
- La primera y más sencilla es ejecutrar ```./maxClique``` directamente. Esta lanza una ejecución con la instancia por defecto (Toy6.clq.txt).
- La segunda es indicarle además la instancia: ```./maxClique instances/Toy4.clq.txt```. En este caso se usa el fichero de configuración por defecto: ```config/GAconfig.txt```.
- La tercera es indicarle además de la instancia, el fichero de configuración: ```./maxClique instances/Toy4.clq.txt config/GAconfig_Toy4.clq.txt```.
- La cuarta es usar el asistente en python con ```./runInstance.py```. Este preguntará qué instancia ejecutar y cuántas veces. Como fichero de configuración utilizará el fichero asociado: ```GAconfig_<instance>.txt```.

---

### Resultados
Los resultados aparecerán por la salida estandar o, si se ejecuta utilizando el script en python, aparecerá en una carpeta llamada ```results``` donde cada archivo se denomina ```results_<instance>.txt```, siendo ```<instance>``` el nombre de la instancia asociada a esos resultados. Además hay un fichero denominado ```globals.txt``` que incorpora los resultados generales de todas las pruebas.
