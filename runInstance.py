#!/usr/bin/python3

import os
import subprocess
from statistics import mean

INSTANCE_PATH = "instances/"
CONFIG_FILE = "config/GAconfig_"
RESULT_PATH = "results/"
RESULT_HEAD = "results"

INT = 0
STR = 1

def menu(inputString,spectedType=INT):
    """
    Menu function to ask for options to the user
    """
    print()
    tryAgain = True
    while tryAgain:
        tryAgain = False
        try:
            if INT == spectedType:
                result = int(input(inputString))
            else:
                result = input(inputString)
        except:
            print("Invalid option, try again")
            tryAgain = True
    return result

# List available instances
i = 0
files = []
for inst in os.listdir(INSTANCE_PATH):
    if inst.endswith(".txt"):
        print(str(i) + "-" + inst)
        files = files + [INSTANCE_PATH + inst]
        i = i+1
print(str(i) + "-" + "ALL")

# Retrieve instance to use
option = menu("Escoge instancia: ")
if option < len(files):
    instanceFilePath = [files[option]]
else:
    instanceFilePath = files

# configChoice = menu("Usar ficheros de configuración específicos? (s/n): ", STR)
# useSpecificCOnfig = (configChoice == 'y' or configChoice == 'Y' or configChoice == 's' or configChoice == 'S')
useSpecificCOnfig = True

# Ask for number of executions to make
runCycles = menu("Introduzca cuantas ejecuciones realizar: ")

congFile = "config/GAconfig.txt"

# Execute algorithm and store the results into files
for inst in instanceFilePath:
    # Config file
    if useSpecificCOnfig:
        congFile = CONFIG_FILE + inst.replace("instances/","")

    # Results file
    fileName = RESULT_HEAD + "_" + inst.replace("instances/","")
    if fileName in os.listdir(RESULT_PATH):
        os.remove(RESULT_PATH + fileName)

    # Open results path
    with open(RESULT_PATH + fileName, "+a") as f:

        fitness = []
        for run in range(runCycles):
            # Run each cycle of the algorithm
            fitness.append(subprocess.call(['./maxClique' , inst, congFile], stdout=f))

        # Print results
        print()
        print(inst,end="  |  ")
        print(congFile,end="  |  ")
        print("runs: {}".format(runCycles),end="  |  ")
        print("fitnesses obtained: " + str(fitness))
        print("\t -> max: {0}, ratioOfMax: {1} %, mean: {2}".format(max(fitness),  (fitness.count(max(fitness))/runCycles)*100,    mean(fitness)))
