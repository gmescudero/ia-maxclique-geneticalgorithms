
#ifndef _max_clique_h_
#define _max_clique_h_

#pragma pack (1)

/* CONFIGURATION FIELDS */

#define CONFIG_FILE "config/GAconfig.txt"
#define GRAPH_DEFAULT_INSTANCE "instances/Toy6.clq.txt"
/* POSIBLE CHOICES
* hamming6-2.clq.txt  
* hamming6-4.clq.txt  
* johnson8-2-4.clq.txt  
* johnson8-4-4.clq.txt  
* keller4.clq.txt  
* p_hat300-1.clq.txt  
* p_hat500-1.clq.txt  
* p_hat300-2.clq.txt  
* p_hat500-2.clq.txt  
* san200_0.7_1.clq.txt  
* MANN_a9.clq.txt  
* Toy4.clq.txt          
* Toy6.clq.txt
* Toy8fix22.clq
* Toy8fix23.clq
*/

#define ARGC_ONE_ARGS   (2) /* One arg passed to the program */
#define ARGC_TWO_ARGS   (2) /* Two arg passed to the program */

#define ARGV_FIRST_ARG  (1) /* First input argument */
#define ARGV_SECON_ARG  (2) /* Second input argument */

#define INACTIVE_NODE   (0) /* Vertex not proposed as part of the clique */
#define ACTIVE_NODE     (1) /* Vertex proposed as part of the clique */

#define NOT_CONNECTED   (0) /* Not conected nodes */
#define CONNECTED       (1) /* Conected nodes */

/* DEFINES */
#define DUMMY_SIZE  (100)   /* Size of dummy buffer */

#define EPSI        (1e-6)  /* Minimum decimal precision */

#define FIRST_INDEX (0)     /* First vector iteration index */

#define ZERO_REAL   (0.0)   /* Zero value of type real */
#define ZERO_INT    (0)     /* One value of type integer */

#define ONE_REAL    (1.0)   /* One value of type real */
#define ONE_INT     (1)     /* One value of type integer */

#define TWO_REAL    (2.0)   /* Two value of type real */

#ifndef FALSE
#define FALSE  (0)  /* False value of type bool */
#endif
#ifndef TRUE
#define TRUE  (1)   /* True value of type bool */
#endif

/* ERROR CODES */

#ifndef OK
#define OK  (0)     /* No error */
#endif
#define NOK (1)     /* Generic error */

#endif /*_max_clique_h_*/
