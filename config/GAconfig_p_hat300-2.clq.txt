rand_seed my_pid
datatype bit

pool_size 128
stop_after 500 use_convergence

crossover simple
x_rate 0.7

mutation simple_invert 
mu_rate 0.9

objective maximize
elitism true

rp_type none
