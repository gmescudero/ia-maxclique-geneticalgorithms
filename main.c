/**
  $HeadURL $
  $Id $

  Project:
    IA_GeneticAlgorithms_MaxClique

  Author:
    GME - German Moreno Escudero
    GAR - Guillermo Aguila Rodriguez
    FAL - Francesco Antonio De Luca

  \file
    main.c

  \brief
    Max clique sover with genetic algorithms

  \details
    This file is the main source file of the max clique solver. It uses
    genetic algorithms given by the library libGA100 to find a solution
    to the max clique problem of a given graph.
*/

/*--- Include files ---*/
#include "ga.h"
#include "maxClique.h"
#include <stdio.h>
#include <string.h>

/*--- Types and Macros ---*/
#define DEBUG (0)

/*--- Function declarations ---*/
int fitnessFunction(Chrom_Ptr chrom);
int readInstance(char *filename);

/*--- Global Variables ---*/
int **graph = NULL; /* Graph nodes relations matrix */
int nnodes; /* Number of nodes */
int nedges; /* Number of edges */

/*--- Main Entry Point ---*/
/**
* \brief
*   Main entry point
*
* \details
*   @DESCRIPTION
*   Main entry point
*
*   This function executes the main entry point of the program.
*
*   @RETURNS
*   - #fitness: returns the fitness of the best solution obtained
*/
int main(int argc, char** argv)
{
  int ret = OK;
  GA_Info_Ptr ga_info;
  int i;
  char *instance  = GRAPH_DEFAULT_INSTANCE;
  char *config    = CONFIG_FILE;

  /* Retrieve arguments */
  if (ARGC_ONE_ARGS <= argc)
  {
    instance = argv[ARGV_FIRST_ARG];
  }
  if (ARGC_TWO_ARGS <= argc)
  {
    config = argv[ARGV_SECON_ARG];
  }

#if DEBUG
  printf("INSTANCE: %s\n",instance);
  printf("CONFIG:   %s\n",config);
#endif

  /* Retrieve instance from file */
  ret = readInstance(instance);

  if (OK == ret)
  {
    /* Initialize the genetic algorithm */
    ga_info = GA_config(config, fitnessFunction);

    /* Set chromosome length from the instance file */
    ga_info->chrom_len = nnodes;

    /* Run the GA */
    GA_run(ga_info);

    /* Print results */
    printf("\tAlgoritm params: {xrate = %g, mrate = %g, population = %d}\n",
      ga_info->x_rate,
      ga_info->mu_rate,
      ga_info->pool_size);
    printf("\tBest chrom: (fitness: %g)\t",
      ga_info->best->fitness);
    for(i = FIRST_INDEX; i < ga_info->chrom_len; i++)
    {
      printf("%d ",(int)ga_info->best->gene[i]);
    }
    printf("\n");
  }

  /* Clean allocated memory */
  for (i = FIRST_INDEX; i < nnodes; i++)
  {
    free(graph[i]);
  }
  free(graph);

  return (int)ga_info->best->fitness;
} /* main */

/*--- Functions Definition ---*/

/**
* \brief
*   Fitness calculation procedure
*
* \details
*   @DESCRIPTION
*   Fitness calculation procedure
*
*   This function calculates the fitness of a given gene for the
*   genetic algorithm.
*
*   @RETURNS
*   - #OK if the procedure executes correctly.
*   - #NOK if there is an error on the execution.
*/
int fitnessFunction(Chrom_Ptr chrom)
{
  int i;
  int j;
  double nodeNum     = ZERO_REAL; /* number of active nodes */
  double cliqueSize  = ZERO_REAL; /* clique size */
  double edgesNum    = ZERO_REAL; /* added edges */
  int isClique       = FALSE;     /* is clique  */

  /* Get the number of nodes in the clique */
  for(i = FIRST_INDEX; i < chrom->length; i++)
  {
    nodeNum += chrom->gene[i];
  }

  /* Calculate the clique size for the number of nodes */
  cliqueSize = nodeNum * ((nodeNum-ONE_REAL)/TWO_REAL);

  /* Calculate the number of edges among all acive nodes */
  for(i = FIRST_INDEX; i < chrom->length; i++)
  {
    if (ACTIVE_NODE == chrom->gene[i])
    {
      for(j = FIRST_INDEX; j < chrom->length; j++)
      {
        if (ACTIVE_NODE == chrom->gene[j])
        {
          edgesNum += graph[i][j];
        }
      }
    }
  }
  edgesNum /= TWO_REAL;

  /* Check if the nodes belong to a clique */
  isClique = (edgesNum == cliqueSize);

#if DEBUG
  printf("nodeNum: %f, cliqueSize: %f, edgesNum: %f\n",nodeNum,cliqueSize,edgesNum);
#endif

  chrom->fitness = (isClique) ? (nodeNum) : ((EPSI+edgesNum)/(EPSI+cliqueSize*nodeNum));

  return OK;
} /* fitnessFunction */

/**
* \brief
*   Instance retrieving from file
*
* \details
*   @DESCRIPTION
*   Instance retrieving from file
*
*   This function retrieves a graph from a file and converts it to a
*   node realtions matrix.
*
*   @RETURNS
*   - #OK if the procedure executes correctly.
*   - #NOK if there is an error on the execution.
*/
int readInstance(char *filename)
{
  int n1;
  int n2;
  FILE *inputf;
  int i;
  int j;

  char dummy1;
  char dummy2[DUMMY_SIZE];
  int  dummy3;

  nnodes = ZERO_INT;
  nedges = ZERO_INT;

  /* Open the file*/
  if( (inputf=fopen(filename,"rt")) == NULL )
  {
    printf( "Cannot open file %s\n",filename);
    exit(NOK);
  }

  /* lee la cabecera */
  fscanf(inputf,"%c %s %d %d\n",&dummy1,dummy2,&nnodes,&nedges);
#if DEBUG
  printf("Opening %s (%d nodes, %d edges)\n",filename,nnodes,nedges);
#endif

  /* Allocate memory for the graph */
  graph = (int**)malloc(nnodes*sizeof(int*));
  for (i = FIRST_INDEX; i < nnodes; i++)
  {
    graph[i] = (int*)malloc(nnodes*sizeof(int));
    for (j = FIRST_INDEX; j < nnodes; j++)
    {
      graph[i][j] = NOT_CONNECTED;
    }
  }

  /* salta la lista de nodos */
  for (i = FIRST_INDEX; i < nnodes; i++)
  {
    fscanf(inputf,"%c  %d %d\n",&dummy1,&dummy3,&dummy3);
  }

  /* lee los edges */
  for(i = FIRST_INDEX; i < nedges; i++)
  {
    fscanf(inputf,"%c %d %d\n",&dummy1,&n1,&n2);
    graph[n1-ONE_INT][n2-ONE_INT] = CONNECTED;
    graph[n2-ONE_INT][n1-ONE_INT] = CONNECTED;
  }

  fclose(inputf);

  /* Print Graph*/
#if DEBUG
  for (i = FIRST_INDEX; i<nnodes; i++)
  {
    printf("[");
    for (j = FIRST_INDEX; j<nnodes; j++)
    {
      printf(" %d ",graph[i][j]);
    }
    printf("]\n");
  }
#endif

  return OK;
} /* readInstance */
